
package bcas.pro.str;

public class StringHomeWork {

	public static void main(String[] args){
		
		String text1 = "Jene";
		String text2 = "Shajiny";
		String text3 = "CSD";
		String text4 = "Programing";
		String text5 = "BCAS Campus";
		String text6 = "Jaffna";
		
		System.out.println(" Q 1 ");
		System.out.println("output \t: " + text1.charAt(1));
		System.out.println();


		System.out.println(" Q 5 ");
		System.out.println("output \t: " + text3.concat(text4));
		System.out.println();


		System.out.println(" Q 17 ");
		System.out.println("output \t: " + text2.indexOf("j", 2));
		System.out.println();


		System.out.println(" Q 18 ");
		System.out.println("output \t: " + text5.indexOf(text6));
		System.out.println();


		System.out.println(" Q 19 ");
		System.out.println("output \t: " + text3.indexOf(text3, 3));
		System.out.println();


		System.out.println(" Q 21 ");
		System.out.println("output \t: " + text6.lastIndexOf("m"));
		System.out.println();


		System.out.println(" Q 22 ");
		System.out.println("output \t: " + text1.lastIndexOf("n", 3));
		System.out.println();


		System.out.println(" Q 23 ");
		System.out.println("output \t: " + text2.lastIndexOf(text2));
		System.out.println();


		System.out.println(" Q 24 ");
		System.out.println("output \t: " + text4.lastIndexOf(text5, 2));
		System.out.println();


		System.out.println(" Q 25");
		System.out.println("output \t: " + text2.length());
		System.out.println();


		System.out.println(" Q 29 ");
		System.out.println("output \t: " + text1.replace("j", "k"));
		System.out.println();


		System.out.println(" Q 35 ");
		System.out.println("output \t: " + text6.startsWith("B"));
		System.out.println();


		System.out.println(" Q 37 ");
		System.out.println("output \t: " + text4.substring(3));
		System.out.println();


		System.out.println(" Q 38 ");
		System.out.println("output \t: " +text3.substring(2, 3));
		System.out.println();


		System.out.println(" Q 40 ");
		System.out.println("output \t: " + text1.toLowerCase());
		System.out.println();


		System.out.println(" Q 43 ");
		System.out.println("output \t: " + text1.toUpperCase());
		System.out.println();


		System.out.println(" Q 45 ");
		System.out.println("output \t: " + text5.trim());
		System.out.println();
		
	}
}

// Output

Q 1 
output 	: e

Q 5 
output 	: CSDPrograming

Q 17 
output 	: 3

Q 18 
output 	: -1

Q 19 
output 	: -1

Q 21 
output 	: -1

Q 22 
output 	: 2

Q 23 
output 	: 0

Q 24 
output 	: -1

Q 25
output 	: 7

Q 29 
output 	: Jene

Q 35 
output 	: false

Q 37 
output 	: graming

Q 38 
output 	: D

Q 40 
output 	: jene

Q 43 
output 	: JENE

Q 45 
output 	: BCAS Campus
