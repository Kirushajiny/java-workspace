import static org.junit.Assert.assertEquals;

import org.junit.Test;

import junit.framework.Assert;

public class FindAreaTest {
	
	FindArea findArea = new FindArea();
	
	@Test
	public void circle_Test() {
		double radious = 7;
		double expected = 153.93804002589985;
		double actual = findArea.circle(radious);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void circle_Test_withZero() {
		
		Assert.assertEquals(0.0, findArea.circle(0));
	}
	
	@Test
	public void rectangle_Test() {
		
		Assert.assertEquals(50.0, findArea.rectangle(10, 5));
		
	}
	
	@Test
	public void SayHi_Test() {
		assertEquals("Hi", findArea.SayHi());
	}
	
	@Test
	public void squre_Test() {
		Assert.assertEquals(100.0, findArea.squre(10.0));
		
	}

}
